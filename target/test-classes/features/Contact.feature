Feature: Contact form

  Background:
    Given I setup driver

  Scenario Outline: Contact form
    When I go to "http://automationpractice.com"
    And I click on Contact us button
    And I select Subject Heading "<subject heading>"
    And I enter email address "<email address>"
    And I enter message "<message text>"

    And I click "Send"


    Examples:
      | subject heading  | email address       | message text |
      | Webmaster        | sadsadsad@yahoo.com | Utilizator1  |
      | Customer service | sadsadad@gmail.com  | Utilizator2  |
      | Customer service |                     | Utilizator3  |

  Scenario Outline: Invalid customer form requests
    When I go to "http://automationpractice.com"
    And I click on Contact us button
    And I select Subject Heading "<subject heading>"
    And I enter email address "<email address>"
    And I enter message "<message text>"
    And I click "Send"
    Then Error message appears "<error message>"

    Examples:
      | subject heading  | email address         | message text | error message                                   |
      | -- Choose --     | test@devschooling.com | text         | Please select a subject from the list provided. |
      | Customer service |                       | text         | Invalid email address.                          |
      | Customer service | test@devschooling.com |              | The message cannot be blank.                    |

  Scenario Outline: Contact form
    When I go to "http://automationpractice.com"
    And I click on Contact us button
    And I select Subject Heading "<subject heading>"
    And I enter email address "<email address>"
    And I enter message "<message text>"
    Then Success message appears "<success message>"

    Examples:
      | subject heading  | email address      | message text | success message                                      |
      | Webmaster        | success1@yahoo.com | Utilizator1  | Your message has been successfully sent to our team. |
      | Customer service | success2@gmail.com | Utilizator2  | Your message has been successfully sent to our team. |
