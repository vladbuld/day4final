package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    private WebDriver webDriver;

    @FindBy(xpath = "//div[@id='contact-link']/a")
    WebElement contactLink;

    public HomePage(WebDriver webDriver) {
        this.webDriver = webDriver;
        // Parseaza aceasta clasa si unde gaseste adnotarea find by stie sa initializeze contact linkul
        PageFactory.initElements(webDriver, this);
    }

    public ContactFormPage clickContactUs() {
        contactLink.click();
        return new ContactFormPage(webDriver);
    }

}
