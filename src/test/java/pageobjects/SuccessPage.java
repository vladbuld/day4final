package pageobjects;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SuccessPage {
    @FindBy(xpath = "//div[@id='center_column']/p")
    private WebElement successMessage;

    public SuccessPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void checkSuccessMessage(String success) {
        Assert.assertEquals(successMessage.getText(), success);
    }
}
