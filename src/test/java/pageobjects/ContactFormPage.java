package pageobjects;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ContactFormPage {

    private WebDriver driver;

    @FindBy(id = "id_contact")
    WebElement subjectHead;

    @FindBy(id = "email")
    private WebElement emailInput;

    @FindBy(id ="message")
    private WebElement messageInput;

    @FindBy(id = "submitMessage")
    private WebElement submitButton;

    @FindBy(xpath = "//*[@id=\"center_column\"]/div/ol/li")
    private WebElement errorMessage;

    public ContactFormPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public ContactFormPage enterEmailAddress(String email) {
        emailInput.sendKeys(email);
        return this;
    }

    public ContactFormPage selectSubjectHeading(String value) {
        Select subjectHeadingDropdown = new Select(subjectHead);
        subjectHeadingDropdown.selectByVisibleText(value);
        return this; // Nu mai e nevoie sa initializam inca o data clasa paginii.
    }

    public ContactFormPage inputMessage(String message) {
        messageInput.sendKeys(message);
        return this;
    }

    public ContactFormPage submitData() {
        submitButton.click();
        return this;
    }

    public ContactFormPage errorMessage(String message){
        Assert.assertEquals(errorMessage.getText(), message);
        return this;
    }

}
