package steps;

import backpack.TestBackPack;
import io.cucumber.java.en.And;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class TestBackPackSteps{
    @Autowired
    private TestBackPack testBackPack;
    @And("I click the testbackpack for message")
    public void iClickTheTestbackpackForMessage(String expecteMessage) {
        Assert.assertEquals(testBackPack.getMessage(), expecteMessage);
    }
}
