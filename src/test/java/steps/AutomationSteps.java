package steps;

import backpack.TestBackPack;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.expression.spel.ast.Selection;
import org.springframework.test.context.ActiveProfiles;
import pageobjects.ContactFormPage;
import pageobjects.HomePage;
import pageobjects.SuccessPage;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Marius Dima on 25.07.2019.
 */

public class AutomationSteps extends TestRunner {

    @Value("${browser.url}")
    private String url;

    @Autowired
    private TestBackPack testBackPack;
    @Autowired
    @Qualifier("chrome")
    private WebDriver driver;
    private static ContactFormPage contactFormPage;
    private static SuccessPage successPage;

    @Given("I setup driver")
    public void iSetupDriver() {

    }

    @And("I go to {string}")
    public void iGoTo(String url) {
        driver.get(this.url);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }


    @When("I search for {string}")
    public void iSearchFor(String item) {
        WebElement searchBox = driver.findElement(By.id("search_query_top"));
        searchBox.clear();
        searchBox.sendKeys(item);
        WebElement submitSearch = driver.findElement(By.xpath("//button[@type='submit']"));
        submitSearch.click();
    }

    @Then("I check that search results have {string}")
    public void iCheckThatSearchResultsHave(String searchedItem) {
        List<WebElement> searchResults = driver.findElements(By.xpath("//div[@class='product-container']// a[@class='product-name']"));
        List<String> productNames = searchResults.stream().map(WebElement::getText).collect(Collectors.toList());
        System.out.println(productNames);
        Assert.assertTrue(productNames.stream().allMatch(name -> name.toLowerCase().contains(searchedItem)));
    }

    @And("I quit the driver")
    public void iQuitTheDriver() {
        driver.quit();
    }


    // Subject heading, email address
    @io.cucumber.java.en.And("I click on Contact us button")
    public void iClickOnContactUsButton() {
        HomePage homePage = new HomePage(driver);
        contactFormPage = homePage.clickContactUs();
    }

    @io.cucumber.java.en.And("I select Subject Heading {string}")
    public void iSelectSubjectHeading(String subjectHeading) {
        contactFormPage.selectSubjectHeading(subjectHeading);
        //ContactUsPage contactUsPage = new ContactUsPage(driver);
        //contactUsPage.selectSubjectHeading(subjectHeading);
    }

    @io.cucumber.java.en.And("I enter email address {string}")
    public void iEnterEmailAddress(String emailAddress) {
        contactFormPage.enterEmailAddress(emailAddress);
    }

    @io.cucumber.java.en.And("I enter message {string}")
    public void iEnterMessage(String message) {
        contactFormPage.inputMessage(message);
        testBackPack.setMessage(message);
    }

    @io.cucumber.java.en.And("I click {string}")
    public void iClick(String arg0) {
        contactFormPage.submitData();
    }

    @io.cucumber.java.en.Then("Error message appears {string}")
    public void errorMessageAppears(String arg0) {
        contactFormPage.errorMessage(arg0);
    }

    @io.cucumber.java.en.Then("Success message appears {string}")
    public void successMessageAppears(String arg0) {
        successPage.checkSuccessMessage(arg0);
    }
}
