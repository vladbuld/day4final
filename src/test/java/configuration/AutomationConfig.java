package configuration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@ComponentScan(basePackages = "backpack")
@Configuration
@PropertySource("classpath:automation.properties")
public class AutomationConfig {
    @Value("${browser.url}")
    private String url;
    @Qualifier("chrome")
    @Bean
    public WebDriver getChrome(){
        System.setProperty("webdriver.chrome.driver", "src/test/resources/driver/chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;
    }
    @Lazy
    @Qualifier("firefox")
    @Bean
    public WebDriver getFirefox(){
        System.setProperty("webdriver.gecko.driver", "src/test/resources/driver/geckodriver.exe");
        FirefoxDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        return driver;
    }
    @Lazy
    @Qualifier("explorer")
    @Bean
    public WebDriver getExplorer(){
        System.setProperty("webdriver.ie.driver", "src/test/resources/driver/IEDriverServer.exe");
        InternetExplorerOptions IEOptions = new InternetExplorerOptions().withInitialBrowserUrl(url);
        InternetExplorerDriver driver = new InternetExplorerDriver(IEOptions);
        driver.manage().window().maximize();
        return driver;
    }
}
